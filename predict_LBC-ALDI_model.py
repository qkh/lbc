# This file runs the LBC video on the Aldi model
# As can be observed the predictions are not good....

import numpy as np
import cv2
import pdb
import matplotlib.pyplot as plt
import os
from cnn.ConvNet import ConvNetClassifier
import time
#import evaluators_LBC as evaluators

MODEL = 'aldi_adaptive_croissant_11_08_2016.json'

cap = cv2.VideoCapture('camera2.mp4')

net = ConvNetClassifier()
net.load_model(MODEL)

print(net.get_product_names())
count=0
while(cap.isOpened()):
    ret, image = cap.read()
     
    image = cv2.resize(image,(480,270))
    cv2.imshow('frame',image)
    #time.sleep(0.5)
    pred = net.predict(image, num_crops = 1)
    print(pred.name,'@', count,' secs')
    count=count+1
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
