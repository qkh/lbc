import cv2
import numpy as np
import os


def mirror(image):
    image = cv2.flip(image, 1)
    return image

def random_mirror(image):
    if np.random.rand(1) > 0.5:
        image = cv2.flip(image, 1)
    return image

def mirror_even(image, index):
    if index % 2 == 0:
        image = cv2.flip(image, 1)
    return image

def random_scale(image, max_scale = 1.5):
    off = (1 + np.random.rand(1)) * (max_scale - 1)
    cv2.resize(image,(image.shape[0]*off,image.shape[1]*off))
    return image

def random_perspective_distort(image, max_distortion):
    h, w,  d = image.shape
    off = np.random.rand(8, 1) * h * max_distortion
    pts1 = np.float32([ [0 + off[0], 0 + off[1]], [0 + off[2], h - off[3]], [w - off[4], off[5]], [w - off[6], h - off[7]] ])
    pts2 = np.float32([[0, 0], [0, h], [w, 0], [w, h]])
    M = cv2.getPerspectiveTransform(pts1, pts2)
    image_warped = cv2.warpPerspective(image, M, (w, h))
    return image_warped

# crops a patch of size crop size from image at random position
def random_crop(image, crop_size):
    h, w, d = image.shape

    if(crop_size[0] > w or crop_size[1] > h):
        return image

    x = np.random.rand(1, 1) * (w-crop_size[0])
    y = np.random.rand(1, 1) * (h-crop_size[1])
    image = image[y:int(y+crop_size[0]),x:int(x+crop_size[1])]
    return image

def center_crop(image, crop_size):
    # crop image
    h, w, d = image.shape

    x = int(np.floor((w-crop_size[0])/2))
    y = int(np.floor((h-crop_size[1])/2))
    image = image[y:int(y+crop_size[1]), x:int(x+crop_size[0])]

    # resize image
    #image = cv2.resize(image, image_patch_size)
    return image


# Distort color by shifting rgb-channels with a random offset
def random_distort_color(image, max_distortion = 0.2):

    # if int image
    image_format = image.dtype
    if image_format == np.uint8:
        max_distortion *= 255
        image = image.astype(np.float32)

    off = -max_distortion + np.random.rand(3, 1) * 2 * max_distortion
    image[:,:,0] = image[:,:,0] + off[0]
    image[:,:,1] = image[:,:,1] + off[1]
    image[:,:,2] = image[:,:,2] + off[2]

    if image_format == np.uint8:
        image[image < 0] = 0
        image[image > 255] = 255
        image = image.astype(np.uint8)
    else:
        image[image < 0] = 0
        image[image > 1] = 1

    return image

def random_rotation(image, min_angle = -180, max_angle = 180):
    center = (int(image.shape[0]/2),int(image.shape[1]/2))
    angle = np.random.rand(1)*(max_angle-min_angle)-min_angle
    R = cv2.getRotationMatrix2D(center, angle, 1.0)
    image = cv2.warpAffine(image,R,(image.shape[0],image.shape[1]))
    return image

def random_noise(image, max_noise = 0.1):
    
    noise_mat = max_noise*np.random.rand(image.shape[0],image.shape[1],image.shape[2])
    image = image + noise_mat

def random_contrast(image, min_contrast = 0.5, max_contrast = 2.0):

    #convert to float image if uint8
    image_format = image.dtype
    if image_format == np.uint8:
        image = image.astype(np.float32)/255.0

    maxIntensity = 1.0 # depends on dtype of image data

    # Parameters for manipulating image data
    phi = 1
    theta = 1

    contrast = np.random.rand(1,1) * (max_contrast - min_contrast) + min_contrast

    # change contrast
    image = (maxIntensity/phi)*(image/(maxIntensity/theta))**contrast

    image[image < 0] = 0
    image[image > 1] = 1

    #convert back to uint8 if it was uint8 before
    if image_format == np.uint8:
        image *= 255
        image = image.astype(np.uint8)

    return image


## add filter2D
def filter_2D(image, kernel_value):
    kernel = (kernel_value, kernel_value);
    kernel = np.ones((kernel_value),np.float32)
    image = cv2.filter2D(image,-1,kernel)
    return image


## add gaussian blur
def random_gaussian_blur(image, kernel_value, max_sigma):
    kernel = (kernel_value, kernel_value);
    sigma = np.random.rand(1, 1) * max_sigma
    image = cv2.GaussianBlur(image, kernel, sigma)
    return image

## add median filter
def median_blur(image, filter_size):
    filter_size = int(filter_size)
    ## median filtering
    image = cv2.medianBlur(image,filter_size)
    return image

# crops a square image one of uniformly distrebuted positions
# index is the index of the possible crop positions 
def uniform_crop_row(image, num_crops, index):

    index = index % num_crops

    if len(image.shape) != 3:
        h, w = image.shape
    else:
        h, w, d = image.shape

    size = h
    centers = np.linspace(size/2, w-size/2, num_crops)
    
    center = centers[index]
    crop_image = image[0:h,int(center-size/2):int(center+size/2)]

    #cv2.imshow("crop",crop_image)
    #cv2.waitKey()
    return crop_image

# divides the image into two rows of crop regions
# uniform crop row is performed for each
def uniform_crop(image, num_crops_total, index, num_rows = 1):

    index = index % num_crops_total

    row_idx = int((index/num_crops_total) * num_rows)
    row_height = int(image.shape[0]/num_rows)
    r1 = row_idx*row_height
    r2 = r1 + row_height

    crop_image = uniform_crop_row(image[r1:r2,:], int(num_crops_total/num_rows), int(index/num_rows))
                                 
    return crop_image

def crop_image(image, top, bottom, left, right):
    if len(image.shape) != 3:
        h, w = image.shape
    else:
        h, w, d = image.shape
    image = image[top:bottom,left:right]
    return image

"""
####################
# TEST and DEBUG AREA
IMAGE_FOLDER = 'aldi/Baguette'
file_list = os.listdir(IMAGE_FOLDER)


for i, file in enumerate(file_list):
    image = cv2.imread(IMAGE_FOLDER + "/" + file, flags=cv2.IMREAD_COLOR)
    #file = file_list[0]
    image = cv2.resize(image,(960, 540))
    image = center_crop(image,(540, 540))
    #create_training_images(image, 10)


    image = random_perspective_distort(image, max_distortion = 0.2)
    image = random_crop(image, crop_size = (540,540))
    image = random_distort_color(image, max_distortion = 0.05)

    cv2.imshow("image", image)


    cv2.waitKey(0)
"""
