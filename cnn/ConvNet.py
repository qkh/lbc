import lasagne
import theano.tensor as T
import theano
import os
import numpy as np
import cv2
import time
import collections
import yaml
import sys
import cnn.model_exporter as model_exporter


class ConvNetBase(object):
    """
        Base Class for Convolutional Neural Network classifier framework.
    """

    def __init__(self, network_config=None):

        self._num_classes = 0
        self._network_dict = None

        # network parameters
        self._network = None
        self._input_var = None
        self._output_var = None
        self._train_fn = None
        self._val_fn = None
        self._lookup = None

        return

    def get_product_names(self):
        return sorted(list(self._lookup['enc_to_name'].values()))

    def get_product_uuids(self):
        return sorted(list(self._lookup['enc_to_uuid'].values()))

    # Performs pre processing steps on the image before prediction
    def _preprocess(self, image):
        # check whether the image loaded is color or gray scale
        if len(image.shape) != 3:
            h, w = image.shape
        else:
            h, w, d = image.shape
            
        # crop center of image
        if(w > h):
            # landscape
            s = h
            x = int((w - s) / 2)
            image = image[0:h, x:s + x]
        elif(w < h):
            # portrait
            s = w
            y = int((h - s) / 2)
            image = image[y:y + s:, 0:w]

        # get input size from network input layer
        input_layer = self._network_dict["Layers"][0]
        input_size = input_layer["shape"]

        image = cv2.resize(image, (input_size[1], input_size[2]))
        if len(image.shape) != 3:
            image = np.expand_dims(image, axis=2)
        return image

    # Build network from network dictionary
    def _build(self, input_var, output_neurons, network_dictionary):

        layers = network_dictionary["Layers"]

        # Input layer
        input_layer = layers[0]
        shape = (None, input_layer["shape"][0], input_layer[
                 "shape"][1], input_layer["shape"][2])
        network = lasagne.layers.InputLayer(shape=shape,
                                            input_var=input_var)
        print(input_layer)
        # Hidden Layers
        for idx, layer in enumerate(layers[1:-1]):
            print(layer)
            if layer["type"] == "Conv2DLayer":
                filter_size = (layer["filter_size"][0], layer["filter_size"][1])
                num_filters = layer["num_filters"]
                nonlinearity = getattr(
                    lasagne.nonlinearities, layer["nonlinearity"])
                network = lasagne.layers.Conv2DLayer(
                    network, num_filters=num_filters, filter_size=filter_size,
                    nonlinearity=nonlinearity, W=lasagne.init.Orthogonal(gain='relu'))
                if 'batch_norm' in layer:
                    if layer['batch_norm'] == "True":
                        network = lasagne.layers.batch_norm(network)

            elif layer["type"] == "MaxPool2DLayer":
                pool_size = (layer["pool_size"][0], layer["pool_size"][1])
                network = lasagne.layers.MaxPool2DLayer(
                    network, pool_size=pool_size)

            elif layer["type"] == "Dropout":
                network = lasagne.layers.dropout(network, p=layer["p"])

            elif layer["type"] == "Dense":
                nonlinearity = getattr(
                    lasagne.nonlinearities, layer["nonlinearity"])
                network = lasagne.layers.DenseLayer(
                    network,
                    W=lasagne.init.Orthogonal(),
                    num_units=layer["num_units"],
                    nonlinearity=nonlinearity)
                if 'batch_norm' in layer:
                    if layer['batch_norm'] == "True":
                        network = lasagne.layers.batch_norm(network)

        # Output layers
        output_layer = layers[-1]
        nonlinearity = getattr(
            lasagne.nonlinearities, output_layer["nonlinearity"])
        network = lasagne.layers.DenseLayer(
            network,
            num_units=output_neurons,
            nonlinearity=nonlinearity)
        print(output_layer)
        print("Network size: ", lasagne.layers.count_params(network))
        return network

    # Compiles the theano graph and functions
    def _compile_network(self, output_neurons):
        # Prepare Theano variables for inputs and targets
        self._input_var = T.tensor4('inputs')
        self._target_var = T.ivector('targets')
        self._learning_rate = T.fscalar('learning_rate')

        # Create neural network model (depending on first command line
        # parameter)
        print("Building model and compiling functions...")
        network = self._build(self._input_var, output_neurons=output_neurons,
                              network_dictionary=self._network_dict)

        # Create a loss expression for training, i.e., a scalar objective we want
        # to minimize (for our multi-class problem, it is the cross-entropy
        # loss):
        prediction = lasagne.layers.get_output(network)
        loss = lasagne.objectives.categorical_crossentropy(
            prediction, self._target_var)
        loss = loss.mean()

        # Create update expressions for training, i.e., how to modify the
        # parameters at each training step. Here, we'll use Stochastic Gradient
        # Descent (SGD) with Nesterov momentum, but Lasagne offers plenty more.
        params = lasagne.layers.get_all_params(network, trainable=True)

        optimizer = getattr(
            lasagne.updates, self._network_dict["Learning"]["optimizer"])
        updates = optimizer(loss, params, learning_rate=self._learning_rate)

        # updates = lasagne.updates.nesterov_momentum(
        #       loss, params, learning_rate=self._learning_rate, momentum=0.9)

        # Create a loss expression for validation/testing. The crucial difference
        # here is that we do a deterministic forward pass through the network,
        # disabling dropout layers.
        test_prediction = lasagne.layers.get_output(
            network, deterministic=True)
        test_loss = lasagne.objectives.categorical_crossentropy(test_prediction,
                                                                self._target_var)
        test_loss = test_loss.mean()
        # As a bonus, also create an expression for the classification
        # accuracy:
        test_acc = T.mean(T.eq(T.argmax(test_prediction, axis=1), self._target_var),
                          dtype=theano.config.floatX)

        test_predicted_class = T.argmax(test_prediction, axis=1)

        self._network = network

        # Compile a function performing a training step on a mini-batch (by giving
        # the updates dictionary) and returning the corresponding training
        # loss:
        self._train_fn = theano.function(
            [self._input_var, self._target_var, self._learning_rate], loss, updates=updates)

        # Compile a second function computing the validation loss and accuracy:
        self._val_fn = theano.function(
            [self._input_var, self._target_var], [test_loss, test_acc])

        # Compile a third function computing the prediction:
        self._pred_fn = theano.function([self._input_var, self._target_var], [
                                        test_predicted_class, test_prediction, test_loss])

    # Normalizes every channel of the images
    # so their pixel mean yields 0 and their pixel standard deviation 1
    def _normalize_intra_image(self, data):
        d0, d1, d2, d3 = data.shape
        data_linear = data.reshape(d0, d1, d2 * d3)
        mu = data_linear.mean(axis=2)
        sigma = data_linear.std(axis=2)
        # data_linear = (data_linear - mu[:,:, np.newaxis]) / sigma[:,:,
        # np.newaxis]
        data_linear -= mu[:, :, np.newaxis]
        data_linear /= sigma[:, :, np.newaxis]
        data = data_linear.reshape(d0, d1, d2, d3)
        return data


class ConvNetClassifier(ConvNetBase):

    """
        Class for classifying Convolutional Neural Network using models
        trained by ConvNetTrainer.
    """

    """ Method predicts the product using a sliding window

        If num_crops is 1, the center of the image is croped
        If num_crops is > 1 a sliding window is used as floowing:
        The input image is cropped using unformly into num_crops
        uniformly and horizontally distributed windows (a ROW of windows)
        Each window is classified and the result the product of propabilities is returned

        Args:
            image: Input image
            num_crops: number of crop windows used for prediction

        Return:
            Result collection

    """
    def predict(self, image,  num_crops = 1):
        if len(image.shape) != 3:
            h, w = image.shape
        else:
            h, w, d = image.shape

        size = h

        propabilities = None
    
        # Sliding window prediction

        # trianglular window weights, windows in the middle have higher weight
        window_weights = np.bartlett(num_crops+2)[1:-1]

        if num_crops > 1:            
            centers = np.linspace(size/2, w-size/2, num_crops)
            for idx, center in enumerate(centers):
                
                crop_image = image[0:h,int(center-size/2):int(center+size/2)]
                _, new_propabilities, _ = self._predict(crop_image)

                # vote relguization to avoid multiplication with 0
                new_propabilities[new_propabilities < 0.1] = 0.1

                if propabilities is None:
                    propabilities = new_propabilities * window_weights[idx]
                else:
                    propabilities *= new_propabilities * window_weights[idx]
                #print(new_propabilities, propabilities, window_weights[idx])
        else:
            center = w/2
            crop_image = image[0:h,int(center-size/2):int(center+size/2)]
            _, propabilities, _ = self._predict(image)

        # renormalize propabilities
        propabilities /= sum(propabilities)

        y_pred = np.argmax(propabilities)
        # Construct Results Data structure
        results = collections.namedtuple(
            "Result", ["name", "names", "prob", "probs", "uuid", "uuids"])
        results.prob = propabilities[y_pred]
        results.probs = propabilities
        results.name = self._lookup['enc_to_name'][str(y_pred)]
        results.names = list(
            self._lookup['enc_to_name'].values())  # sort this
        results.uuid = self._lookup['enc_to_uuid'][str(y_pred)]
        results.uuids = list(
            self._lookup['enc_to_uuid'].values())  # sort this

        # sort names alphabet
        results.uuids = [
            x for (y, x) in sorted(zip(results.names, results.uuids))]
        results.names = sorted(results.names)

        return results

    def _predict(self, image):
        """ 
            Internal predict method with preprocessing
        """
        input_layer = self._network_dict["Layers"][0]
        # check for the input layer shape
        # if its 1 - Gray scale image
        # if its 3 - color image
        if input_layer["shape"][0] == 1:
            image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        image = self._preprocess(image)

        X_test = np.transpose(image, (2, 0, 1))
        X_test = np.expand_dims(X_test, axis=0)
        X_test = X_test / np.float32(256)
        X_test = self._normalize_intra_image(X_test)

        # create dummy target variable, as it is needed by val_fn
        y_test = np.zeros((X_test.shape[0],), dtype=np.uint8)

        y_pred, propabilities, loss = self._pred_fn(X_test, y_test)
        y_pred = int(y_pred)

        propabilities = propabilities[0]

        return y_pred, propabilities, loss

    def load_model(self, model_file):
        """
            Method loads a model file and builds the network and sets the saved network weights
            This method is useful when using a trained model for prediction
    
        Args:
            model_file:
                model

        Example:
            image = cv2.imread(image_file)
            network = CNNPredictor()
            network.load_model(model_name)
            result = network.predict(image)
            print(result.name)

        """
        print("Loading model", model_file)
        # data = pickle.load(open(model_file, "rb"), encoding='latin1')
        data = model_exporter.load_model(model_file)

        weights = data["weights"]
        self._lookup = data["lookup"]
        self._num_classes = data["num_classes"]
        self._network_dict = data["network_dict"]

        # build network
        self._compile_network(self._num_classes)

        # set all network weights
        lasagne.layers.set_all_param_values(self._network, weights)





class ConvNetTrainer(ConvNetBase):

    """
        Class for training Convolutional Neural Network and building models.
    """

    def build_network(self, network_config, num_classes):
        """
            Methods builds network from network config
        """
        self._network_dict = self._load_network_config(network_config)
        self._compile_network(output_neurons = num_classes)

    def train(self, data_folder, num_epochs = 50, output_file = 'model.json'):
        """ Method trains a Convolutional Neural Network from images stored in data_folder

        Args:
            data_folder: Input data folder, folder must have following structure
                - train/
                    (product_folders)/(training images)
                - validation/
                    (product_folders)/(validation images)
                - test/
                    (product_folders)/(test images)

            num_epochs: Number of epochs to train
            output_model: file_name of the json model to be saved

        """

        # Generate lookup
        class_folder_list = os.listdir(data_folder + '/train')
        class_folder_list.sort()
        self._lookup = self._generate_lookup(class_folder_list)

        self._num_classes = len(class_folder_list)
        print("Number of classes:", self._num_classes)

        # Build network if not build already
        # if self._network is None:
        #    self._compile_network(self._num_classes)

         # Load the dataset
        print("Loading data...")
        X_train, y_train, X_val, y_val, X_test, y_test = self._load_dataset(data_folder)


        # initial training rate
        learning_rate = self._network_dict["Learning"]["learning_rate_start"]
        learning_rate_decay = self._network_dict["Learning"]["learning_rate_decay"]
        learning_rate_min = self._network_dict["Learning"]["learning_rate_min"]

        if 'mini_batch_size' in self._network_dict["Learning"].keys():
            mini_batch_size = self._network_dict["Learning"]["mini_batch_size"]
        else:
            mini_batch_size = 50 # default size


        # Finally, launch the training loop.
        print("Starting training...")

        print("learning_rate",learning_rate)
        print("learning_rate_decay",learning_rate_decay)
        print("learning_rate_min",learning_rate_min)
        print("mini_batch_size",mini_batch_size)

        # We iterate over epochs:
        for epoch in range(num_epochs):
            # In each epoch, we do a full pass over the training data:
            train_err = 0
            train_batches = 0
            start_time = time.time()

          

            for batch in self._iterate_minibatches(X_train, y_train, mini_batch_size, shuffle=True):
                inputs, targets = batch
                train_err += self._train_fn(inputs, targets, learning_rate)
                train_batches += 1

            # And a full pass over the validation data:
            val_err = 0
            val_acc = 0
            val_batches = 0
            for batch in self._iterate_minibatches(X_val, y_val, mini_batch_size, shuffle=False):
                inputs, targets = batch
                err, acc = self._val_fn(inputs, targets)
                val_err += err
                val_acc += acc
                val_batches += 1

            # update learning rate
            learning_rate = max([learning_rate * learning_rate_decay, learning_rate_min])

            # Then we print the results for this epoch:
            print("Epoch {} of {} took {:.3f}s".format(
                epoch + 1, num_epochs, time.time() - start_time))
            print("  learning rate:\t\t{:.6f}".format(learning_rate))
            print("  training loss:\t\t{:.6f}".format(train_err / train_batches))
            print("  validation loss:\t\t{:.6f}".format(val_err / val_batches))
            print("  validation accuracy:\t\t{:.2f} %".format(
                val_acc / val_batches * 100))



            # Save network after each epoch
            self._save_model(output_file)

        # After training, we compute and print the test error:
        test_err = 0
        test_acc = 0
        test_batches = 0
        for batch in self._iterate_minibatches(X_test, y_test, 30, shuffle=False):
            inputs, targets = batch
            err, acc = self._val_fn(inputs, targets)
            test_err += err
            test_acc += acc
            test_batches += 1
        print("Final results:")
        print("  test loss:\t\t\t{:.6f}".format(test_err / test_batches))
        print("  test accuracy:\t\t{:.2f} %".format(
            test_acc / test_batches * 100))


    def transfer_weights(self, source_model_file, update_layers = None):
        """
            Method transfers weights from a pretrained source model 
            for the type of layers specified update_layers = []
            If update_layers is not specified, all layers will be updated

            The the order and size of the layers to be transfered must be the
            same in the model and the source_model.

        """

        # data = pickle.load(open(source_model_file, "rb"), encoding='latin1')
        data = model_exporter.load_model(source_model_file)
        weights = data["weights"]

        layers = lasagne.layers.get_all_layers(self._network)
        layer_names = [l.__class__.__name__ for l in layers]

        if update_layers == None:
            # all layers
            update_layers = set(layer_names)
        print("l", self._network)
        params = lasagne.layers.get_all_params(layers)

        i = 0
        for p, w, l in zip(params, weights, layer_names):
                if l in update_layers:
                    if p.get_value().shape != w.shape:
                        raise ValueError("mismatch: parameter has shape %r but value to "
                                         "set has shape %r" %
                                         (p.get_value().shape, w.shape))
                    else:
                        p.set_value(w)
                        print("Update layer", i, ":", l)
                i += 1

         # This is just a simple helper function iterating over training data in
    # mini-batches of a particular size, optionally in random order. It assumes
    # data is available as numpy arrays.
    def _iterate_minibatches(self,inputs, targets, batchsize, shuffle=False):
        assert len(inputs) == len(targets)
        if shuffle:
            indices = np.arange(len(inputs))
            np.random.shuffle(indices)
        for start_idx in range(0, len(inputs) - batchsize + 1, batchsize):
            if shuffle:
                excerpt = indices[start_idx:start_idx + batchsize]
            else:
                excerpt = slice(start_idx, start_idx + batchsize)
            yield inputs[excerpt], targets[excerpt]


    def _save_model(self, file_name):

        weights = lasagne.layers.get_all_param_values(self._network)
        data = {
                    "weights": weights,
                    "lookup": self._lookup,
                    "num_classes": self._num_classes,
                    "network_dict": self._network_dict
                }
        # save with pickle protocol 2 for compability with python 2
        # pickle.dump(data, open(file_name, "wb"), protocol=2)
        model_exporter.save_model(data,file_name)

    # Loads image data set from a folder, folder structure must be:
    # ('folder/train/, folder/validation, folder/test)
    def _load_dataset(self,folder):

        def _load_images(source_folder):
            labels = None
            data = None

            class_folder_list = os.listdir(source_folder)
            class_folder_list.sort()

            # pre allocate data
            size_data = sum([len(os.listdir(source_folder + '/' + folder)) for folder in class_folder_list])
            input_layer = self._network_dict["Layers"][0]
            shape = (size_data, input_layer["shape"][0],input_layer["shape"][1],input_layer["shape"][2])
            data = np.zeros(shape,dtype=np.float32)
            jx = 0
            for idx, class_folder in enumerate(class_folder_list):
                print(idx, class_folder)
                file_list = os.listdir(source_folder + '/' + class_folder)
                num_files = len(file_list)
                class_lables = np.ones(num_files).astype(np.uint8) * idx

                if labels is None:
                    labels = class_lables
                else:
                    labels = np.hstack((labels, class_lables))

                for file in file_list:
                    image_path = source_folder + '/' + class_folder + '/' + file
                    image = cv2.imread(image_path, flags=cv2.IMREAD_COLOR)
                    # if the input layer is 1, convert the image to grayscale
                    if input_layer["shape"][0] == 1:
                        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
                    image = self._preprocess(image)
                    image = np.transpose(image, (2, 0, 1))
                    image = np.expand_dims(image, axis=0)
                    data[jx,:,:,:] = image
                    jx += 1
            print("Data size:", data.shape)
            return data / np.float32(256), labels


        X_train, y_train = _load_images(folder + '/train')
        X_val, y_val = _load_images(folder + '/validation')
        X_test, y_test = _load_images(folder + '/test')

        # remove mean of each image
        print("Preprocessing, removing image mean")

        X_train = self._normalize_intra_image(X_train)
        X_val = self._normalize_intra_image(X_val)
        X_test = self._normalize_intra_image(X_test)

        print("Train size:", len(y_train))
        print("Validiation size:", len(y_val))
        print("Test size:", len(y_test))

        # We just return all the arrays in order, as expected in main().
        # (It doesn't matter how we do this as long as we can read them again.)
        return X_train, y_train, X_val, y_val, X_test, y_test

            # Generates lookup table for product names and uid
    def _generate_lookup(self, class_folder_list):
        lookup = dict()

        enc_to_uuid = dict()
        enc_to_name = dict()

        for i, item in enumerate(class_folder_list):
            # folders have following format "product name_productid"
            string_items = item.split("_")
            enc_to_name[str(i)] = string_items[0] 
            if len(string_items) > 1:
                enc_to_uuid[str(i)] = string_items[1]
            else:
                # uuid must be unique
                enc_to_uuid[str(i)] = "no_value" + str(i) 
        lookup['enc_to_uuid'] = enc_to_uuid
        lookup['enc_to_name'] = enc_to_name

        return lookup

    # Load network defined in YAML file
    def _load_network_config(self, network_config):
        with open(network_config, "r") as f:
            network_dictionary = yaml.load(f)
            return network_dictionary

  


