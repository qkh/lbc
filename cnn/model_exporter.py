import json
import numpy as np
import base64
import pickle
import sys
import os

def Base64EncodeData(input):
    """
        Recursively iterates through a collection of dictionaries and lists and encodes numpy ndarrays into base 64 strings.
    """
    if isinstance(input, dict):
        return dict((Base64EncodeData(key), Base64EncodeData(value)) for key, value in input.items())
    elif isinstance(input, list):
        return [Base64EncodeData(element) for element in input]
    elif isinstance(input, np.ndarray):
        return ["/b64ndarray", str(input.dtype), base64.b64encode(input).decode(encoding='UTF-8'), input.shape]
    else:
        return input


def Base64DecodeData(input):
    """
        Recursively iterates through a collection of dictionaries and lists and
        decodes base64 encoded strings back into numpy ndarrays.
    """
    if isinstance(input, dict):
        return dict((Base64DecodeData(key), Base64DecodeData(value)) for key, value in input.items())
    elif isinstance(input, list):
        if input[0] == "/b64ndarray":
            # decode
            dtype = np.dtype(input[1])
            arr = np.frombuffer(base64.decodestring(input[2].encode('UTF-8')), dtype)
            if len(input) > 3:
                return arr.reshape(input[3])
            return arr
        else:
            return [Base64DecodeData(element) for element in input]
    else:
        return input



def save_model(data, file_name):
    """
        Save model wrapper

        Args:
            param1 (dict): model data as dictionary
            param2 (str): save file name

    """
    data = Base64EncodeData(data)
    with open(file_name,'w') as f:
        json.dump(data, f)

def load_model(file_name):
    """
        Load model wrapper

        Args:
            param1 (str): load file name
        Returns:
           model data
    """
    
    with open(file_name,'r') as f:
        data = json.loads(f.read())
        data = Base64DecodeData(data)

    return data


def pickle_to_json(pickle_file, json_file):
    """
        Helper function for converting legacy pickle format to new json format
    """
    if sys.version_info >= (3, 0):
        data = pickle.load(open(pickle_file, encoding = "latin1"))
    else:
        data = pickle.load(open(pickle_file))
    save_model(data, json_file)


def latin1_to_utf8(input):
    """
        Helper function for converting latin1 encoding to utf8 encoding
    """
    if isinstance(input, dict):
        return dict((latin1_to_utf8(key), latin1_to_utf8(value)) for key, value in input.items())
    elif isinstance(input, list):
        return [latin1_to_utf8(element) for element in input]
    elif isinstance(input, str):
        return input.encode("latin1").decode("UTF-8")
    else:
        return input
