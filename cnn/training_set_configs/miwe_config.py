import cv2
import cnn.data_augmentation as data_augmentation

# Config for generating training set from data set
# This config script must implement create_training_images() and create_test_images()

SETTINGS = {
    "input_folder": 'data/data_sets/miwe_all',
    "output_folder": 'data/training_sets/miwe_128_misch',    
    # Split data into [train, validation, test]
    "data_split": [0.5, 0.3, 0.2],
    # Maximum amount of images in class
    "data_max_limit": 2000,
    # Minimum amount of images in class
    "data_min_limit": 1,
    "output_image_size": (128,128),
    # sort images before split
    "sort" : False
}

# Creates multiple augmented training images from the input image
# Must return a list of images
def create_training_images(input_image):

    image_list = list()

    image = cv2.resize(input_image,(960, 540))

    num_crops = 5

    # top image
    image_top = data_augmentation.crop_image(image.copy(), top = 0, bottom = 270, left = 100, right = 860)
    #image_top = cv2.resize(image_top,(960, 640))

    # bottom image
    image_bottom = data_augmentation.crop_image(image.copy(), top = 250, bottom = 540, left = 100, right = 860)
    #image_bottom = cv2.resize(image_bottom,(960, 640))

    for image in [image_top,image_bottom]:
        for i in range(0,num_crops):
            augmented_image = data_augmentation.uniform_crop_row(image.copy(), num_crops = num_crops, index = i)
            image_list.append(cv2.resize(augmented_image.copy(),SETTINGS["output_image_size"]))
            image_list.append(cv2.resize(data_augmentation.mirror(augmented_image.copy()),SETTINGS["output_image_size"]))
            augmented_image = data_augmentation.random_scale(augmented_image, max_scale = 1.5)
            augmented_image = data_augmentation.random_perspective_distort(augmented_image, max_distortion = 0.20)
            augmented_image = data_augmentation.random_distort_color(augmented_image, max_distortion = 0.05) 
            augmented_image = data_augmentation.random_contrast(augmented_image, min_contrast = 0.5, max_contrast = 1.5)
            augmented_image = cv2.resize(augmented_image, SETTINGS["output_image_size"])
            image_list.append(augmented_image)
            image_list.append(data_augmentation.mirror(augmented_image.copy()))

    return image_list

# Creates multiple augmented test images from the input image
# Must return a list of images
def create_test_images(input_image):

    image_list = list()

    image = cv2.resize(input_image.copy(),(960, 540))

    # top image
    image_top = data_augmentation.crop_image(image.copy(), top = 0, bottom = 270, left = 100, right = 860)
    #image_top = cv2.resize(image_top,(960, 640))

    # bottom image
    image_bottom = data_augmentation.crop_image(image.copy(), top = 270, bottom = 540, left = 100, right = 860)
    #image_bottom = cv2.resize(image_bottom,(960, 640))

    num_crops = 3
    for image in [image_top,image_bottom]:
        for i in range(0,num_crops):
            test_image = data_augmentation.uniform_crop_row(image.copy(), num_crops = num_crops, index = i)
            test_image = cv2.resize(test_image,SETTINGS["output_image_size"])
            image_list.append(test_image)
            image_list.append(data_augmentation.mirror(test_image.copy()))
        
    return image_list