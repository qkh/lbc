import cv2
import cnn.data_augmentation as data_augmentation

# Config for generating training set from data set
# This config script must implement create_training_images() and create_test_images()

SETTINGS = {
    # "input_folder": 'data/data_sets/aldi_new_annotations',
    "input_folder": '/home/qkh/annotations/LBC',
    "output_folder": 'data/training_sets/LBC_128',

   # "class_blacklist": ['Bun-BAXTER_b14b311a7726d5d6aa165c2d31fd1fe8','Empty-BAXTER_9c4b67c3724830e6adb8938b3b7e6003','Croissant-BAXTER_b14b311a7726d5d6aa165c2d31fd1bc9','Undecided_d6a443cc31391ba155f1b5fc301eab16','Easy Clean_d0ac8f1141d6b859142ba5006793e338'],

    # Split data into [train, validation, test]
    "data_split": [0.7, 0.2, 0.1],

    # Maximum amount of images in class
    "data_max_limit": 500,

    # Minimum amount of images in class
    "data_min_limit": 50,
    "output_image_size": (128, 128),

    # sort images before split
    "sort": True
}


def create_training_images(input_image):
    image_list = list()

    image = cv2.resize(input_image, (960, 540))

    num_crops = 1

    num_augmentations = 1

    for i in range(0, num_crops):
        augmented_image_crop = data_augmentation.center_crop(image, crop_size=(540, 540))
        image_list.append(cv2.resize(augmented_image_crop.copy(), SETTINGS["output_image_size"]))
        image_list.append(cv2.resize(data_augmentation.mirror(augmented_image_crop.copy()), SETTINGS["output_image_size"]))

        for i in range(0, num_augmentations):
            augmented_image = data_augmentation.random_perspective_distort(augmented_image_crop.copy(), max_distortion = 0.15)
            augmented_image = data_augmentation.random_distort_color(augmented_image, max_distortion=0.04)
            augmented_image = data_augmentation.random_contrast(augmented_image, min_contrast=0.5, max_contrast=1.5)
            augmented_image = cv2.resize(augmented_image, SETTINGS["output_image_size"])

            image_list.append(augmented_image)
            image_list.append(data_augmentation.mirror(augmented_image.copy()))

    return image_list


def create_test_images(input_image):
    """
    Creates multiple augmented test images from the input image
    Must return a list of images
    """

    image_list = list()

    image = cv2.resize(input_image, (960, 540))

    num_crops = 1

    for i in range(0, num_crops):
        test_image = data_augmentation.center_crop(image, crop_size=(540, 540))
        test_image = cv2.resize(test_image, SETTINGS["output_image_size"])
        image_list.append(test_image)
        image_list.append(data_augmentation.mirror(test_image.copy()))

    return image_list
