import cv2
import cnn.data_augmentation as data_augmentation

# Config for generating training set from data set
# This config script must implement create_training_images() and create_test_images()

SETTINGS = {
    "input_folder": 'data/data_sets/rational_brot1_3d_5',
    "output_folder": 'data/training_sets/rational_brot1_128_3d_8',    
    # Split data into [train, validation, test]
    "data_split": [0.6, 0.3, 0.1],
    # Maximum amount of images in class
    "data_max_limit": 10000,
    # Minimum amount of images in class
    "data_min_limit": 1,
    "output_image_size": (128,128),
    # sort images before split
    "sort" : False
}

# Creates multiple augmented training images from the input image
# Must return a list of images
def create_training_images(input_image):

    image_list = list()

    image = cv2.resize(input_image,(960, 540))
    #image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    num_crops = 1

    # top image
    #image_top = data_augmentation.crop_image(image.copy(), top = 0, bottom = 270, left = 100, right = 860)
    #image_top = cv2.resize(image_top,(960, 640))

    # bottom image
    image_bottom = data_augmentation.crop_image(image.copy(), top = 200, bottom = 500, left = 200 right = 700)
    image_bottom = cv2.resize(image_bottom,(540, 540))

    num_augmentations = 1

    for image in [image_bottom]:
        for i in range(0,num_crops):
            augmented_image_crop = data_augmentation.uniform_crop_row(image.copy(), num_crops = num_crops, index = i)
            image_list.append(cv2.resize(augmented_image_crop.copy(),SETTINGS["output_image_size"]))
            image_list.append(cv2.resize(data_augmentation.mirror(augmented_image_crop.copy()),SETTINGS["output_image_size"]))
            for i in range(0,num_augmentations):
                crop_shape = (int(augmented_image_crop.shape[0]*0.90),int(augmented_image_crop.shape[1]*0.90))
                augmented_image = data_augmentation.random_crop(augmented_image_crop.copy(), crop_shape) #translate jitter
                #augmented_image = data_augmentation.random_rotation(augmented_image_crop.copy(),min_angle = 5, max_angle = 5)
                #augmented_image = data_augmentation.random_scale(augmented_image, max_scale = 1.1)
                augmented_image = data_augmentation.random_perspective_distort(augmented_image_crop.copy(), max_distortion = 0.10)
                augmented_image = data_augmentation.random_distort_color(augmented_image, max_distortion = 0.03) 
                augmented_image = data_augmentation.random_contrast(augmented_image, min_contrast = 0.5, max_contrast = 1.5)
                augmented_image = cv2.resize(augmented_image, SETTINGS["output_image_size"])
                image_list.append(augmented_image)
                image_list.append(data_augmentation.mirror(augmented_image.copy()))

        return image_list

# Creates multiple augmented test images from the input image
# Must return a list of images
def create_test_images(input_image):

    image_list = list()

    image = cv2.resize(input_image.copy(),(960, 540))
    #image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # top image
    #image_top = data_augmentation.crop_image(image.copy(), top = 0, bottom = 270, left = 100, right = 860)
    #image_top = cv2.resize(image_top,(960, 640))

    # bottom image
    image_bottom = data_augmentation.crop_image(image.copy(), top = 200, bottom = 500, left = 200, right = 700)
    image_bottom = cv2.resize(image_bottom,(540, 540))

    num_crops = 1
    for image in [image_bottom]:
        for i in range(0,num_crops):
            test_image = data_augmentation.uniform_crop_row(image.copy(), num_crops = num_crops, index = i)
            test_image = cv2.resize(test_image,SETTINGS["output_image_size"])
            image_list.append(test_image)
            image_list.append(data_augmentation.mirror(test_image.copy()))
        
    return image_list