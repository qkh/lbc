import cv2
import cnn.data_augmentation as data_augmentation

# Config for generating training set from data set
# This config script must implement create_training_images() and create_test_images()


SETTINGS = {
    # "input_folder": 'data/data_sets/aldi_new_annotations',
    "input_folder": 'data/data_sets/baxter_oven_rack_2',
    "output_folder": 'data/training_sets/baxter_rack_scanner_128_13_mix',


    # Split data into [train, validation, test]
    "data_split": [0.8, 0.1, 0.1],

    # Maximum amount of images in class
    "data_max_limit": 10000,

    # Minimum amount of images in class
    "data_min_limit": 100,
    "output_image_size": (128, 128),

    # sort images before split
    "sort": False
}


def create_training_images(input_image):
    image_list = list()

    image = cv2.resize(input_image, (400, 600))

    #crop
    image = image[150:550,0:400]

    num_crops = 1

    num_augmentations = 2

    for i in range(0, num_crops):
        augmented_image_crop = data_augmentation.center_crop(image, crop_size=(400, 400))
        image_list.append(cv2.resize(augmented_image_crop.copy(), SETTINGS["output_image_size"]))
        image_list.append(cv2.resize(data_augmentation.mirror(augmented_image_crop.copy()), SETTINGS["output_image_size"]))

        for i in range(0, num_augmentations):
            augmented_image = data_augmentation.random_perspective_distort(augmented_image_crop.copy(), max_distortion = 0.3)
            augmented_image = data_augmentation.random_distort_color(augmented_image, max_distortion=0.04)
            augmented_image = data_augmentation.random_contrast(augmented_image, min_contrast=0.5, max_contrast=2)
            augmented_image = cv2.resize(augmented_image, SETTINGS["output_image_size"])

            image_list.append(augmented_image)
            image_list.append(data_augmentation.mirror(augmented_image.copy()))

    return image_list


def create_test_images(input_image):
    """
    Creates multiple augmented test images from the input image
    Must return a list of images
    """

    image_list = list()

    image = cv2.resize(input_image, (400, 600))

    #crop
    image = image[150:550,0:400]

    num_crops = 1

    for i in range(0, num_crops):
        test_image = data_augmentation.center_crop(image, crop_size=(400, 400))
        test_image = cv2.resize(test_image, SETTINGS["output_image_size"])
        image_list.append(test_image)
        image_list.append(data_augmentation.mirror(test_image.copy()))

    return image_list